import { Injectable } from '@angular/core';
import { Router } from '@angular/router';

@Injectable()
export class AppLoginService {


  public get isAdmin(){
    return localStorage.getItem("isAdmin") === "true";
  }
  public get isLogged(){
    return localStorage.getItem("isLogged") === "true";
  }
  public get userName(){
    return localStorage.getItem("userName");
  }
  constructor(private router: Router) {
   }

   login(isAdmin: boolean, userName: string)
   {
     localStorage.setItem("isLogged", "true")
     localStorage.setItem("userName", userName);
     localStorage.setItem("isAdmin", isAdmin === true ? "true": "false")
   }
   logout()
   {
     localStorage.removeItem("isLogged");
     localStorage.removeItem("isAdmin");
     this.router.navigate(['/login']);
   }
   


}
