import { Injectable } from '@angular/core';
import { Router, CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { AppLoginService } from './app-login.service';
 
@Injectable()
export class AuthGuard implements CanActivate {
 
    constructor(private router: Router, private appLoginSerivce: AppLoginService) { }
 
    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        if (this.appLoginSerivce.isLogged) {
            return true;
        }
 
        this.router.navigate(['/login'], { queryParams: { returnUrl: state.url }});
        return false;
    }
}

@Injectable()
export class AuthAdminGuard implements CanActivate {
 
    constructor(private router: Router, private appLoginSerivce: AppLoginService) { }
 
    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        if (this.appLoginSerivce.isLogged && this.appLoginSerivce.isAdmin) {
            return true;
        }
 
        this.router.navigate(['/login'], { queryParams: { returnUrl: state.url }});
        return false;
    }
}