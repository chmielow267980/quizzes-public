import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { Subject } from 'rxjs/Subject';
import { of } from 'rxjs/observable/of';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { catchError, map, tap } from 'rxjs/operators';
import { MessageService } from '../message.service';
import { QuizDetailsComponent } from '../quiz-details/quiz-details.component';
import { QuizzesService } from '../quizzes.service';
import {
  debounceTime, distinctUntilChanged, switchMap
} from 'rxjs/operators';
import { AppLoginService } from '../app-login.service';


const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};



@Component({
  selector: 'app-quiz-search',
  templateUrl: './quiz-search.component.html',
  styleUrls: ['./quiz-search.component.css']
})
export class QuizSearchComponent implements OnInit {
  quizzes$: any;

  private searchTerms = new Subject<string>();

constructor(private quizzesService: QuizzesService) {}
  ngOnInit(): void {

    this.quizzes$ = this.searchTerms.pipe(
      debounceTime(300),

      distinctUntilChanged(),

      switchMap((term: string) => this.quizzesService.searchQuizzes(term)),
    );
  }
  search(term: string): void {
    this.searchTerms.next(term);
  }
}
