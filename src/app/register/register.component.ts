import { Component, OnInit } from '@angular/core';
import { AppLoginService } from '../app-login.service';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {
  model: any = {};
  error: any;
  errorMsg: any;

  isLoging = true;
  constructor(private http: HttpClient, private appLoginSerivce: AppLoginService) { 

  }

  ngOnInit() {
  }

  register() {
  if(!this.model.password)
  {
    this.error = true;
    this.errorMsg = "Hasło nie może być puste";
    return;
  }
  if(!this.model.repassword)
  {
    this.error = true;
    this.errorMsg = "Powtórzenie hasła nie może być puste";
    return;
  }
  if(!this.model.username)
  {
    this.error = true;
    this.errorMsg = "Nazwa użytkonwika nie może być pusta";
    return;
  }
   if(this.model.password !== this.model.repassword)
   {
     this.error = true;
     this.errorMsg = "Hasło i powtórzenie nie pasują"
     return;
   }
   var m = {
    userName: this.model.username,
    password: this.model.password
   }
   this.error = false;
   this
   .http
   .post("api/users", m)
   .subscribe((values: any[]) => {
     this.isLoging = false;
   }, (error) => {
     console.error(error);
   });

  }

}
