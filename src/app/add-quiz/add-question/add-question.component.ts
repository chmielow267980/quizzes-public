import { Component, OnInit, Input } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { FormArray } from '@angular/forms/src/model';

@Component({
  selector: 'app-add-question',
  templateUrl: './add-question.component.html',
  styleUrls: ['./add-question.component.css']
})
export class AddQuestionComponent implements OnInit {

  @Input() group: FormGroup;
  
  constructor(private _fb: FormBuilder) { }

  ngOnInit() {
  }

  addAnswer(){
    const control = <FormArray>this.group.controls['answers'];
    var q = this._fb.group({
      answer: ['', Validators.required],
      isCorrect: [false]
    });
    
    control.push(q);
  }

  removeAnswer(i: any)
  {
    const control = <FormArray>this.group.controls['answers'];
    if(control.controls.length >= 2)
    {
      control.removeAt(i);
    }
  }

}
