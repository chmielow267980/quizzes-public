
import { Component, Input } from '@angular/core';
import { FormGroup }        from '@angular/forms';
import { OnInit } from '@angular/core/src/metadata/lifecycle_hooks';
 
@Component({
  selector: 'app-question',
  templateUrl: './question.component.html',
  styleUrls: ['./question.component.css']
})
export class QuestionComponent implements OnInit {
  ngOnInit(): void {
  }

  @Input() question: any;
  @Input() form: any;
  
  constructor() { }

  get isValid() 
  {  
    var isValid = false;
    Object.keys(this.form.controls).forEach(key => {
      var val = this.form.controls[key].value;
      if(val.isChecked)
      {
        isValid = true;
      }
   });
   if (isValid === false)
   {
    this.form.setErrors({duplicate: true});
   }
   else
   {
     this.form.setErrors(null);
   }
    return isValid;
  }

  checkAnswer(a: any)
  {
    var aa = this.question;
    Object.keys(this.form.controls).forEach(key => {
      var val = this.form.controls[key].value;
      if(a.id === val.id)
      {
        a.isChecked = !a.isChecked;
      }
  });
  }
}