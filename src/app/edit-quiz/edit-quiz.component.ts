import { Component, OnInit, Input } from '@angular/core';
import { AppLoginService } from '../app-login.service';
import { ActivatedRoute } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { FormControl, FormGroup, Validators, FormBuilder } from '@angular/forms';
import { debug } from 'util';
import { RequiredValidator } from '@angular/forms/src/directives/validators';
import { QuizzesService } from '../quizzes.service';
import { OnDestroy } from '@angular/core/src/metadata/lifecycle_hooks';


@Component({
  selector: 'app-edit-quiz',
  templateUrl: './edit-quiz.component.html',
  styleUrls: ['./edit-quiz.component.css']
})
export class EditQuizComponent implements OnInit, OnDestroy {
  
  id: number;
  private sub: any;
  quizDetails: any;
  form: any;
  payLoad = "";
  isSummary = false;
  totalPoints;
  userPoints;
  addComentForm: any;
  showForm = true;
  comments: any;

  constructor(private _fb: FormBuilder, private http: HttpClient, private route: ActivatedRoute, public appLoginSerivce: AppLoginService) {}

  ngOnInit() {
    this.comments = [];
    this.sub = this.route.params.subscribe(params => {
      this.id = +params['id'];
      this
      .http
      .get("api/quizzes/2")
      .subscribe((value: any) => {
        this.quizDetails = value;
        this.totalPoints = value.questions.length;
        var userLogin = this.appLoginSerivce.userName;
        this.addComentForm = this._fb.group({
          nick: [{value: userLogin, disabled: this.appLoginSerivce.isLogged}, [Validators.required]],
          comment: ['', [Validators.required]],
          quizId: [this.quizDetails.id, [Validators.required]]
      });

      this.getComments();
      this.form = this.toFormGroup();
      }, (error) => {
        console.error(error);
      });
   });
   
  }

  getComments(): any {
    this
    .http
    .get("api/quizzesComment?quizId=" + this.id)
    .subscribe((value: any) => {
      this.comments = value;
    }, (error) => {
      console.error(error);
    });

  }

  ngOnDestroy(): void {
    this.sub.unsubscribe();
  }

  toFormGroup() {
    let group: any = {};
    this.quizDetails.questions.forEach(question => {
      question.isValid = false;
      let qGroup: any = {};
      question.answers.forEach(a => {
        a.isChecked = false;
        qGroup[a.id] = new FormControl(a);
      })
      group[question.id] = new FormGroup(qGroup);
    });
    return new FormGroup(group);
  }

  onSubmit() {
    var model = [];
    Object.keys(this.form.controls).forEach(key => {
      var answers = [];
      Object.keys(this.form.controls[key].controls).forEach(subKey => { 
        var val = this.form.controls[key].controls[subKey].value;
        if(val.isChecked)
        {
          answers.push(val.id);
        }
        
      });
      model.push({
        questionId: key,
        answers: answers
      });
  });
    this.calculateResult(model);
  }
 
  calculateResult(model: any[]) {
    this.userPoints = 0;
    for(var i =0; i < this.totalPoints; i++)
    {
      var correctAnswers = [];
      for(var j =0; j < this.quizDetails.questions[i].answers.length; j++)
      {
          if (this.quizDetails.questions[i].answers[j].isCorrect)
          {
            correctAnswers.push(this.quizDetails.questions[i].answers[j].id)
          }
      }
      var userAnswers = model[i].answers;
      var point = this.answersEqual(correctAnswers, userAnswers);
      if(point)
      {
        this.userPoints++;
      }
    }

    this.isSummary = true;
    this.showForm = true;

  }
   answersEqual(arr1: any, arr2: any) {
    if(arr1.length !== arr2.length)
        return false;
    for(var i = 0; i < arr1.length; i++) {
        if(arr1[i] !== arr2[i])
            return false;
    }

    return true;
  }

  addComent(form: any){
    var model = {
      quizId: form.controls.quizId.value,
      nick: form.controls.nick.value,
      comment: form.controls.comment.value,
    };
    this.http
      .post("api/quizzesComment", model)
      .subscribe((value) => {
        this.showForm = false;
      }, (error) => {
        console.error(error);
      });
  }
}