import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientInMemoryWebApiModule } from 'angular-in-memory-web-api';
import { QuizService } from './in-memory-data.service';
import { RouterModule, Routes } from '@angular/router';
import { AppComponent } from './app.component';
import { HttpClientModule } from '@angular/common/http';
import { QuizComponent } from './quiz/quiz.component';
import { appRoutes } from './app.routing';
import { QuizDetailsComponent } from './quiz-details/quiz-details.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { QuestionComponent } from './quiz-details/question/question.component';
import { LoginComponent } from './login/login.component';
import { AppLoginService } from './app-login.service';
import { AddQuizComponent } from './add-quiz/add-quiz.component';
import { AuthGuard, AuthAdminGuard } from './auth.guard';
import { AddQuestionComponent } from './add-quiz/add-question/add-question.component';
import { AddQuestionAnswerComponent } from './add-quiz/add-question-answer/add-question-answer.component';
import { RegisterComponent } from './register/register.component';
import { AdminPanelComponent } from './admin-panel/admin-panel.component';
import { QuizSearchComponent } from './quiz-search/quiz-search.component';
import { MessagesComponent } from './messages/messages.component';
import { MessageService } from './message.service';
import { QuizzesService } from './quizzes.service';
import { EditQuizComponent } from './edit-quiz/edit-quiz.component';
import { AnswerCorrectComponent } from './quiz-details/answer-correct/answer-correct.component';

@NgModule({
  declarations: [
    AppComponent,
    QuizComponent,
    QuizDetailsComponent,
    QuestionComponent,
    LoginComponent,
    AddQuizComponent,
    AddQuestionComponent,
    AddQuestionAnswerComponent,
    RegisterComponent,
    AdminPanelComponent,
    QuizSearchComponent,
    MessagesComponent,
    EditQuizComponent,
    AnswerCorrectComponent
  ],
  imports: [
    HttpClientModule,
    HttpClientInMemoryWebApiModule.forRoot(
      QuizService, { dataEncapsulation: false }
    ),
    RouterModule.forRoot(appRoutes),
    BrowserModule,
    FormsModule,
    ReactiveFormsModule
  ],
  providers: [AppLoginService, AuthGuard, AuthAdminGuard, MessageService, QuizzesService],
  bootstrap: [AppComponent]
})
export class AppModule { }
