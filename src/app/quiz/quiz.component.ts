import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ActivatedRoute, Router } from '@angular/router';
import { AppLoginService } from '../app-login.service';
import { QuizzesService } from '../quizzes.service';

@Component({
  selector: 'app-quiz',
  templateUrl: './quiz.component.html',
  styleUrls: ['./quiz.component.css']
})
export class QuizComponent implements OnInit {

  quizzes: any;
  selectedQuiz: any;
  constructor( private router: Router, private appLoginSerivce: AppLoginService, private quizzesService: QuizzesService){
  }

  ngOnInit(): void {
    this.getQuizzes();
    }

    getQuizzes(): void {
      this.quizzesService.getQuizzes()
      .subscribe((values: any[]) => {
        this.quizzes = [];
        if(!this.appLoginSerivce.isLogged)
        {
            values.forEach(element => {
              if(!element.isPrivate)
              {
                  this.quizzes.push(element);
              }
            });
        }
        else{
          var userName = this.appLoginSerivce.userName;
          values.forEach(element => {
            if(element.isPrivate)
            {
              if(userName === element.userName)
                  this.quizzes.push(element);
            }
            else{
              this.quizzes.push(element);
            }
          });
        }
      }, (error) => {
        console.error(error);
      });
    }
    goToQuiz()
    {
      if(this.selectedQuiz)
        this.router.navigate(['/quiz', this.selectedQuiz.id]);
    }

}
