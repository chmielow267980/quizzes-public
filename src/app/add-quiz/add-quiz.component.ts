import { Component, OnInit } from '@angular/core';
import { FormGroup, FormArray, FormBuilder, Validators } from '@angular/forms';
import { HttpClient } from '@angular/common/http';
import { AppLoginService } from '../app-login.service';
import { QuizzesService } from '../quizzes.service';


@Component({
  selector: 'app-add-quiz',
  templateUrl: './add-quiz.component.html',
  styleUrls: ['./add-quiz.component.css']
})
export class AddQuizComponent implements OnInit {

  public myForm: FormGroup;
  added = false;
  constructor(private _fb: FormBuilder, private http: HttpClient, public appLoginSerivce: AppLoginService) { }

  ngOnInit() {
    this.myForm = this._fb.group({
      name: ['', [Validators.required]],
      description: ['', [Validators.required]],
      isPrivate: [false],
      questions: this._fb.array([])
  });
  this.addQuestion();

  }

  initQuestion() {
    var q = this._fb.group({
        question: ['', Validators.required],
        answers: this._fb.array([])
    });
    const control = <FormArray>q.controls['answers'];
    control.push(this.addAnswers());
    return q;
}

addAnswers(){
  var q = this._fb.group({
    answer: ['', Validators.required],
    isCorrect: [false]
  });
   
  return q;
}
addQuestion() {
    const control = <FormArray>this.myForm.controls['questions'];
    const addrCtrl = this.initQuestion();
    
    control.push(addrCtrl);
}

removeAddress(i: number) {
    const control = <FormArray>this.myForm.controls['questions'];
    control.removeAt(i);
}

save(data: any) {
  var model = {
    name: data.controls['name'].value,
    description: data.controls['description'].value,
    isPrivate: data.controls['isPrivate'].value,
    userName: this.appLoginSerivce.userName,
    questions:[]
  };
  const control = <FormArray>data.controls['questions'];
  for(var i=0; i < control.controls.length; i++){
    const ctrlQ = <FormArray>control.controls[i];
    model.questions[i] = {
      id: i + 1,
      question: ctrlQ.controls['question'].value,
      answers: []
    }
    for(var j= 0; j < ctrlQ.controls['answers'].controls.length; j++)
    {
      var val = ctrlQ.controls['answers'].controls[j];
      model.questions[i].answers.push({
        id: j + 1,
        answer: val.controls['answer'].value,
        isCorrect: val.controls['isCorrect'].value,
      })
    }
  }
  this.http
      .post("api/quizzes", model)
      .subscribe((value) => {
        this.added = true;
      }, (error) => {
        console.error(error);
      });

}





}
