import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AnswerCorrectComponent } from './answer-correct.component';

describe('AnswerCorrectComponent', () => {
  let component: AnswerCorrectComponent;
  let fixture: ComponentFixture<AnswerCorrectComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AnswerCorrectComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AnswerCorrectComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
