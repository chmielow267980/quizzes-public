import { InMemoryDbService } from 'angular-in-memory-web-api';

export class QuizService implements InMemoryDbService {
  createDb() {
    const quizzes = [
      {
        id: 1, name: 'Systemy operacyjne ',
        isPrivate: true,
        userName: "user",
        description: 'Realizacja szeregowania (planowania) zadań w Linux.',
        questions: [
          { id: 1, question: 'W systemie Linux dostępne są następujące algorytmy szeregowania zadań', answers: [{ id: 1, answer: "statyczny SCHED_OTHER i dynamiczne FIFO i Round Robin", isCorrect: false }, { id: 2, answer: " statyczny SCHED_OTHER i dynamiczny FIFO", isCorrect: false }, { id: 3, answer: "statyczne FIFO i Round Robin i dynamiczny SCHED_OTHER", isCorrect: true }, { id: 4, answer: " statyczny FIFO i dynamiczne SCHED_OTHER i Round Robin", isCorrect: false }] },
          { id: 2, question: 'Jest prawdą, że w systemie Linux szeregowaniu zadań w trybie dynamicznym pierwsze zadanie do wykonania to:', answers: [{ id: 1, answer: " zadanie o największej wartości nice", isCorrect: false }, { id: 2, answer: " zadanie, które ma najwięcej niewykorzystanych impulsów zegara w bieżącej epoce", isCorrect: false }, { id: 3, answer: "zadanie o najwyższym priorytecie dynamicznym", isCorrect: true }, { id: 4, answer: "które ma największą wartość atrybutu interactive credit", isCorrect: false }] },
          { id: 3, question: 'Prawdą jest, że szeregowanie procesów w systemie Linux cechuje następująca własność: ', answers: [{ id: 1, answer: " wyznaczanie epoki następuje po zakończeniu wykonywania procesów czasu ", isCorrect: false }, { id: 2, answer: "w pierwszej kolejności w zakresie dynamicznym wykonywane są procesy zgodnie z regułą SJF", isCorrect: false }, { id: 3, answer: " istnieje wsparcie dla aplikacji interaktywnych, które wykorzystuje sygnały od klawiatury i myszki", isCorrect: false }, { id: 4, answer: "zadania o priorytecie statycznym podlegają wywłaszczeniu.", isCorrect: true }] },
        ]
      },
      {
        id: 2, name: 'Bazy danych',
        description: 'Rodzaje baz danych',
        questions: [
          { id: 1, question: 'Prawdą jest, że: ', answers: [{ id: 1, answer: "obiektowe bazy danych pozwalają na przechowywanie danych w tabelach", isCorrect: false }, { id: 2, answer: "aktywne bazy danych czekają na operacje zadane przez użytkownika", isCorrect: false }, { id: 3, answer: "bazy analityczne mogą służyć do magazynowania nadchodzących danych", isCorrect: false }, { id: 4, answer: "bazy relacyjno-obiektowe pozwalają na definicję struktur", isCorrect: true }] },
          { id: 2, question: ' Prawdą jest, że: ', answers: [{ id: 1, answer: " zapytanie w strumieniowych bazach danych może obejmować stale wydłużający się okres", isCorrect: true }, { id: 2, answer: " bazy relacyjno-obiektowe mają rozszerzenia GIS", isCorrect: false }, { id: 3, answer: " bazy temporalne to rodzaj baz służących do przechowywania danych tymczasowych", isCorrect: false }, { id: 4, answer: "bazy relacyjne wymagają przeszukiwania całej tabeli", isCorrect: false }] },
          { id: 3, question: ' Prawdą jest, że: ', answers: [{ id: 1, answer: "aktywne bazy danych czekają na operacje zadane przez użytkownika", isCorrect: false }, { id: 2, answer: " obiektowe bazy danych mają ustandaryzowany protokół dostępu (język)", isCorrect: false }, { id: 3, answer: " hurtownie danych to rodzaj baz, gdzie tworzone są kolejne wirtualne instancje baz danych", isCorrect: false }, { id: 4, answer: "relacyjne bazy danych oparte są o koncepcję zbiorów encji", isCorrect: true }] },
        ]
      },
      {
        id: 3, name: 'Bazy danych 2',
        description: 'Rodzaje baz danych',
        isPrivate: true,
        userName: "user",
        questions: [
          { id: 1, question: 'Prawdą jest, że: ', answers: [{ id: 1, answer: "obiektowe bazy danych pozwalają na przechowywanie danych w tabelach", isCorrect: false }, { id: 2, answer: "aktywne bazy danych czekają na operacje zadane przez użytkownika", isCorrect: false }, { id: 3, answer: "bazy analityczne mogą służyć do magazynowania nadchodzących danych", isCorrect: false }, { id: 4, answer: "bazy relacyjno-obiektowe pozwalają na definicję struktur", isCorrect: true }] },
          { id: 2, question: ' Prawdą jest, że: ', answers: [{ id: 1, answer: " zapytanie w strumieniowych bazach danych może obejmować stale wydłużający się okres", isCorrect: true }, { id: 2, answer: " bazy relacyjno-obiektowe mają rozszerzenia GIS", isCorrect: false }, { id: 3, answer: " bazy temporalne to rodzaj baz służących do przechowywania danych tymczasowych", isCorrect: false }, { id: 4, answer: "bazy relacyjne wymagają przeszukiwania całej tabeli", isCorrect: false }] },
          { id: 3, question: ' Prawdą jest, że: ', answers: [{ id: 1, answer: "aktywne bazy danych czekają na operacje zadane przez użytkownika", isCorrect: false }, { id: 2, answer: " obiektowe bazy danych mają ustandaryzowany protokół dostępu (język)", isCorrect: false }, { id: 3, answer: " hurtownie danych to rodzaj baz, gdzie tworzone są kolejne wirtualne instancje baz danych", isCorrect: false }, { id: 4, answer: "relacyjne bazy danych oparte są o koncepcję zbiorów encji", isCorrect: true }] },
        ]
      },
      {
        id: 4, name: 'Systemy operacyjne 2',
        description: 'Realizacja szeregowania (planowania) zadań w Windows NT',
        questions: [
          { id: 1, question: ' Prawdziwy jest następujący opis dotyczący zasad szeregowania zadań w systemie Windows NT:', answers: [{ id: 1, answer: " aplikacje pierwszoplanowe mają priorytet 15,", isCorrect: false }, { id: 2, answer: "doładowanie podnosi priorytet procesom serwerowym, oczekującym na dostęp do HDD", isCorrect: true }, { id: 3, answer: "  połowa niewykorzystanego czasu jest dodawana przy następnym uzyskaniu procesora", isCorrect: false }, { id: 4, answer: "jeśli proces wykorzystał swój przedział czasowy, to obniżana jest wartość jego priorytetu o 1,", isCorrect: false }] },
          { id: 2, question: ' Prawdziwy jest następujący opis dotyczący zasad szeregowania zadań w systemieWindows NT:', answers: [{ id: 1, answer: "procesy serwerowe mają dwa razy dłuższe czasy wykonania niż procesy w tle,", isCorrect: true }, { id: 2, answer: " proces w tle może mieć krótszy czas wykonania niż procesy aktywne,", isCorrect: false }, { id: 3, answer: " proces interaktywny jest wspierany przez mechanizm ochrony przed zagłodzeniem,", isCorrect: false }, { id: 4, answer: " proces, który wykonał się poprzez przydzielony czas procesora ma skracany czas wykonania w następnym przedziale.", isCorrect: false }] },
          { id: 3, question: ' Prawdziwy jest następujący opis dotyczący zasad szeregowania zadań w systemieWindows NT:', answers: [{ id: 1, answer: "procesy, które mają wyższy priorytet czekają na zakończenie aktualnie ", isCorrect: false }, { id: 2, answer: " procesy, które mają niższy priorytet niż aktualnie wykonujące się, muszą czekać na zakończenie wykonywania procesów o wyższych priorytetach,", isCorrect: true }, { id: 3, answer: " proces, który dostał zasób, na który czekał, dostaje jednorazowo priorytet 15,", isCorrect: false }, { id: 4, answer: "procesy serwerowe w systemach serwerowych mają dłuższe przedziały wykonywania.", isCorrect: false }] },
        ]
      },

    ];

    const quizzesComment = [
      { id: 1, quizId: 1, nick: "user", comment: "super test!" }
    ];

    const users = [
      { id: 1, userName: "user", password: "password" }
    ]
    return { quizzes, quizzesComment, users };
  }
}