import { NgModule } from '@angular/core';
import { Routes } from '@angular/router';
import { QuizComponent } from './quiz/quiz.component';
import { AppComponent } from './app.component';
import { QuizDetailsComponent } from './quiz-details/quiz-details.component';
import { LoginComponent } from './login/login.component';
import { AddQuizComponent } from './add-quiz/add-quiz.component';
import { AuthGuard, AuthAdminGuard } from './auth.guard';
import { RegisterComponent } from './register/register.component';
import { AdminPanelComponent } from './admin-panel/admin-panel.component';
import { EditQuizComponent } from './edit-quiz/edit-quiz.component';

export const appRoutes: Routes = [
  { path: 'quiz/:id', component: QuizDetailsComponent },
  { path: '', redirectTo: '/quiz', pathMatch: 'full' },
  { path: 'quiz', component: QuizComponent },
  { path: 'login', component: LoginComponent },
  { path: 'register', component: RegisterComponent },
  { path: 'addQuiz', component: AddQuizComponent,  canActivate: [ AuthGuard ] },
  { path: 'adminPanel', component: AdminPanelComponent,  canActivate: [ AuthAdminGuard ] },
  { path: 'editQuiz', component: EditQuizComponent }

];