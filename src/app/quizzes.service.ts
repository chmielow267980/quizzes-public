import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

import { catchError, map, tap } from 'rxjs/operators';
import { Observable } from 'rxjs/Observable';
import { of } from 'rxjs/observable/of';
import { QuizDetailsComponent } from './quiz-details/quiz-details.component';
import { MessageService } from './message.service';

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};


@Injectable()
export class QuizzesService {
 
  constructor(private http: HttpClient,
    private messageService: MessageService) { }
    private handleError<T> (operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
 
    
      console.error(error);


      this.log(`${operation} failed: ${error.message}`);


      return of(result as T);
    };
  }


  private log(message: string) {
    this.messageService.add('HeroService: ' + message);
  }
    getQuizzes (): any {
      return this
      .http
      .get("api/quizzes").pipe(
        tap(quizzes => this.log(`fetched quizzes`)),
        catchError(this.handleError('getQuizzes', []))
      );

    }
    searchQuizzes(term: string): any {
      if (!term.trim()) {

        return of([]);
      }
      return this.http.get(`api/quizzes/?name=${term}`).pipe(
        tap(_ => this.log(`found heroes matching "${term}"`)),
        catchError(this.handleError('searchQuizzes', []))
      );
    }

    deleteQuiz (quiz: any | number): any {
      const id = typeof quiz === 'number' ? quiz : quiz.id;
      const url = `${"api/quizzes"}/${id}`;
        return this.http.delete(url, httpOptions).pipe(
        tap(_ => this.log(`deleted quiz id=${id}`)),
        catchError(this.handleError('deleteQuiz'))
      );
    }

    updateQuiz (quiz: any): any {
      return this.http.put("api/quizzes", quiz, httpOptions).pipe(
        tap(_ => this.log(`updated hero id=${quiz.id}`)),
        catchError(this.handleError<any>('updateHero'))
      );
    }
}
