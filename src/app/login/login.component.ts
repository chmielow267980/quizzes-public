import { Component, OnInit } from '@angular/core';
import { AppLoginService } from '../app-login.service';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  model: any = {};
  error: any;
  isLoging = true;
  constructor(private http: HttpClient, private appLoginSerivce: AppLoginService) { 

  }

  ngOnInit() {
  }

  login() {
    if(this.model.username == 'admin' && this.model.password == 'admin')
    {
      this.error =false;
      this.appLoginSerivce.login(true, this.model.username);
      this.isLoging = false;
      return;
    }
    this.http.get("api/users?userName=" + this.model.username)
    .subscribe((value: any) => {
      if(value && value.length > 0)
      {
        if(value[0].password === this.model.password)
        {
          this.isLoging = false;
          this.error = false;
          this.appLoginSerivce.login(false, this.model.username);
        }
        else{
          this.isLoging = true;
          this.error = true;
        }
      }
      else
      {
        this.isLoging = true;
        this.error = true;
      }
      }, (error) => {
      console.error(error);
    });
    
  }

}
