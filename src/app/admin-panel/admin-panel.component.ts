import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { AppLoginService } from '../app-login.service';
import { QuizzesService } from '../quizzes.service';
import { Input } from '@angular/core/src/metadata/directives';


@Component({
  selector: 'app-admin-panel',
  templateUrl: './admin-panel.component.html',
  styleUrls: ['./admin-panel.component.css']
})
export class AdminPanelComponent implements OnInit {

  constructor(private http: HttpClient, private appLoginSerivce: AppLoginService, private quizzesService: QuizzesService) { }

  quizzes: any;
  ngOnInit(): void {
    this
      .http
      .get("api/quizzes")
      .subscribe((values: any[]) => {
        this.quizzes = values;
      }, (error) => {
        console.error(error);
      });
    }

    setPublic(quizId){
        var quiz = this.quizzes[quizId];
        quiz.isPrivate = false;
        this.http
      .post("api/quizzes", quiz)
      .subscribe((values: any[]) => {
      }, (error) => {
        console.error(error);
      });
    }

    delete(quizId): void {
      var quiz = this.quizzes[quizId];
      this.quizzes = this.quizzes.filter(h => h !== quiz);
      this.quizzesService.deleteQuiz(quiz).subscribe();
    }



}
